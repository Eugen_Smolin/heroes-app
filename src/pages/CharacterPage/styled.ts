import styled from 'styled-components';

import { device } from 'utils/device';

const Wrap = styled.div`
  display: flex;
  grid-gap: 50px;
  margin-top: 50px;

  @media ${device.md} {
    margin-top: 0;
    flex-direction: column;
  }
`;

const ImageWrap = styled.div`
  flex: 0 0 300px;

  @media ${device.md} {
    align-self: center;
    max-width: 300px;
    width: 100%;
    height: 300px;
  }
`;

const Name = styled.h1`
  font-weight: 700;
  text-transform: uppercase;
  font-size: 22px;
  margin-bottom: 25px;
`;

const Description = styled.div`
  font-size: 18px;
  max-width: 550px;

  @media ${device.md} {
    max-width: none;
    text-align: justify;
  }
`;

export { Wrap, ImageWrap, Name, Description };
