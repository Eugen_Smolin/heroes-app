import { useEffect } from 'react';
import { useHistory } from 'react-router-dom';

import { Loader } from 'components/Loader';
import { ComicsCard } from 'components/ComicsCard';
import { Pagination } from 'components/Pagination';
import { EmptyList } from 'components/EmptyList';
import { AdvertisingBanner } from 'components/AdvertisingBanner';
import { useAppDispatch, useAppSelector } from 'hooks/redux-hook';
import { useMediaQuery } from 'hooks/useMediaQuery';
import { device } from 'utils/device';
import { Status } from 'utils/const';
import { comics } from 'store/comics';
import { Wrap, ItemsWrap } from './styled';

function ComicsPage() {
  const history = useHistory();
  const dispatch = useAppDispatch();
  const list = useAppSelector(comics.selectors.list);
  const pagination = useAppSelector(comics.selectors.pagination);
  const fetchingStatus = useAppSelector(comics.selectors.fetchingStatus);

  const isMobileDevice = useMediaQuery(device.xsm);
  const isMediumDevice = useMediaQuery(device.md);

  useEffect(() => {
    dispatch(comics.thunks.getAllComics({ offset: pagination.offset }));
  }, []);

  const goToComics = (id: number) => () => history.push(`comics/${id}`);

  const renderComics = () => {
    if (fetchingStatus === Status.IDLE || fetchingStatus === Status.PENDING) {
      return <Loader />;
    }

    if (!list.length) return <EmptyList />;

    return list.map((comics) => {
      return (
        <ComicsCard
          key={comics.id}
          onClick={goToComics(comics.id)}
          {...comics}
        />
      );
    });
  };

  const changePage = (page: number, offset: number) => {
    dispatch(comics.actions.SET_PAGINATION({ page, offset }));
  };

  const getData = (offset: number) => {
    dispatch(comics.thunks.getAllComics({ offset }));
  };

  return (
    <Wrap>
      {!isMediumDevice && <AdvertisingBanner />}
      <ItemsWrap>{renderComics()}</ItemsWrap>
      {list.length ? (
        <Pagination
          {...pagination}
          showLessItems={isMobileDevice}
          disabled={fetchingStatus === Status.PENDING}
          changeCurrentPageAction={changePage}
          getDataAction={getData}
        />
      ) : null}
    </Wrap>
  );
}

export default ComicsPage;
