import styled from 'styled-components';
import { Link } from 'react-router-dom';

import { device } from 'utils/device';

const Wrap = styled.div`
  position: relative;
  display: grid;
  grid-template-columns: 293px 1fr 80px;
  grid-column-gap: 50px;
  min-height: 300px;
  margin-top: 50px;

  @media ${device.md} {
    display: block;
    margin-top: 0;
  }
`;

const StyledLink = styled(Link)`
  font-size: 18px;
  font-weight: 700;
  color: ${({ theme }) => theme.black}};
`;

const ImageWrap = styled.div`
  width: 100%;
  height: 450px;

  @media ${device.md} {
    margin-bottom: 25px;

    & > div {
      display: block;
      margin: 0 auto;
      max-width: 300px;
      width: 100%;
    }
  }
`;

const Name = styled.div`
  font-size: 22px;
  font-weight: 700;

  @media ${device.md} {
    display: flex;
    justify-content: space-between;
    column-gap: 20px;

    ${StyledLink} {
      flex-shrink: 0;
    }
  }
`;

const Description = styled.div`
  font-size: 18px;
  margin-top: 25px;
`;

const Price = styled.div`
  color: ${({ theme }) => theme.red};
  font-size: 24px;
  font-weight: 700;
  margin-top: 25px;
  text-transform: uppercase;
`;

export { Wrap, ImageWrap, Name, StyledLink, Description, Price };
