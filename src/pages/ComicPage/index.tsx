import { useEffect } from 'react';
import { useParams } from 'react-router-dom';

import { Image } from 'components/Image';
import { Loader } from 'components/Loader';
import { AdvertisingBanner } from 'components/AdvertisingBanner';
import { useAppDispatch, useAppSelector } from 'hooks/redux-hook';
import { useMediaQuery } from 'hooks/useMediaQuery';
import { device } from 'utils/device';
import { Status } from 'utils/const';
import { comic } from 'store/comics/comic';
import {
  ImageWrap,
  Wrap,
  StyledLink,
  Name,
  Description,
  Price,
} from './styled';

function ComicPage() {
  const { id } = useParams<{ id: string }>();
  const dispatch = useAppDispatch();
  const data = useAppSelector(comic.selectors.data);
  const fetchingStatus = useAppSelector(comic.selectors.fetchingStatus);

  const isMobileDevice = useMediaQuery(device.md);

  useEffect(() => {
    dispatch(comic.thunks.getComicById(id));
  }, []);

  const content =
    fetchingStatus === Status.IDLE || fetchingStatus === Status.PENDING ? (
      <Loader />
    ) : !data ? null : (
      <>
        <ImageWrap>
          <Image
            src={data.imageHref}
            alt={data.title}
          />
        </ImageWrap>
        <div>
          <Name>
            {data.title}
            {isMobileDevice && (
              <StyledLink to="/comics">Back to all</StyledLink>
            )}
          </Name>
          <Description>{data.description}</Description>
          <Description>{data.pageCount} pages</Description>
          <Description>Language: {data.language}</Description>
          <Price>{data.price}</Price>
        </div>
        {!isMobileDevice && <StyledLink to="/comics">Back to all</StyledLink>}
      </>
    );

  return (
    <>
      {!isMobileDevice && <AdvertisingBanner />}
      <Wrap>{content}</Wrap>
    </>
  );
}

export default ComicPage;
