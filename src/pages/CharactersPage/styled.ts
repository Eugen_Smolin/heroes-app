import styled from 'styled-components';

import { device } from 'utils/device';

const Wrap = styled.div`
  display: grid;
  grid-row-gap: 50px;
  grid-column-gap: 25px;
  grid-template-columns: 1fr 360px;
  align-items: start;

  @media ${device.lg} {
    display: block;
  }
`;

const ItemsWrap = styled.div`
  position: relative;
  min-height: 666px;
  display: grid;
  grid-column-gap: 25px;
  grid-row-gap: 30px;
  grid-template-columns: repeat(3, 1fr);

  @media ${device.sm} {
    grid-template-columns: repeat(2, 1fr);
  }

  @media ${device.xsm} {
    grid-template-columns: 1fr;
  }
`;

export { Wrap, ItemsWrap };
