import { useEffect } from 'react';
import { useHistory } from 'react-router-dom';

import { Loader } from 'components/Loader';
import { Banner } from 'components/CharactersPage/Banner';
import { CharacterCard } from 'components/CharactersPage/CharacterCard';
import { Pagination } from 'components/Pagination';
import { SideInfo } from 'components/CharactersPage/SideInfo';
import { SearchForm } from 'components/CharactersPage/SearchForm';
import { EmptyList } from 'components/EmptyList';
import { useAppDispatch, useAppSelector } from 'hooks/redux-hook';
import { useMediaQuery } from 'hooks/useMediaQuery';
import { Status } from 'utils/const';
import { device } from 'utils/device';
import { characters } from 'store/characters';
import { character } from 'store/characters/character';
import { Wrap, ItemsWrap } from './styled';

function CharactersPage() {
  const history = useHistory();
  const dispatch = useAppDispatch();
  const list = useAppSelector(characters.selectors.list);
  const pagination = useAppSelector(characters.selectors.pagination);
  const fetchingStatus = useAppSelector(characters.selectors.fetchingStatus);

  const isTabletDevice = useMediaQuery(device.lg);
  const isMobileDevice = useMediaQuery(device.xsm);

  useEffect(() => {
    dispatch(characters.thunks.getAllCharacters({ offset: pagination.offset }));
  }, []);

  const getCharacterInfo = (id: number) => {
    if (!isTabletDevice) {
      dispatch(character.thunks.getCharacterById(id));
    } else {
      history.push(`/characters/${id}`);
    }
  };

  const renderCharacters = () => {
    if (fetchingStatus === Status.IDLE || fetchingStatus === Status.PENDING) {
      return <Loader />;
    }

    if (!list.length) return <EmptyList />;

    return list.map((character) => {
      return (
        <CharacterCard
          onClick={getCharacterInfo}
          key={character.id}
          {...character}
        />
      );
    });
  };

  const changePage = (page: number, offset: number) => {
    dispatch(characters.actions.SET_PAGINATION({ page, offset }));
  };

  const getData = (offset: number) => {
    dispatch(characters.thunks.getAllCharacters({ offset }));
  };

  return (
    <Wrap>
      <Banner css={{ gridColumn: '1/3' }} />
      {isTabletDevice && <SearchForm />}
      <div>
        <ItemsWrap>{renderCharacters()}</ItemsWrap>
        {list.length ? (
          <Pagination
            {...pagination}
            showLessItems={isMobileDevice}
            disabled={fetchingStatus === Status.PENDING}
            changeCurrentPageAction={changePage}
            getDataAction={getData}
          />
        ) : null}
      </div>
      {!isTabletDevice && (
        <div>
          <SideInfo />
          <SearchForm />
        </div>
      )}
    </Wrap>
  );
}

export default CharactersPage;
