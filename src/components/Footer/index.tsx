import { Wrap } from './styled';

export function Footer() {
  const year = new Date().getFullYear();

  return (
    <Wrap>
      <a href="http://marvel.com">Data provided by Marvel. © {year} MARVEL</a>
    </Wrap>
  );
}
