import styled from 'styled-components';

const Wrap = styled.div`
  display: flex;
  justify-content: center;
  margin-top: 50px;

  .rc-pagination {
    list-style: none;
    display: flex;
    column-gap: 8px;
    user-select: none;

    &-item {
      display: flex;
      justify-content: center;
      align-items: center;
      min-width: 30px;
      min-height: 30px;
      padding: 4px;
      cursor: pointer;
      background-color: ${({ theme }) => theme.grey};
      color: ${({ theme }) => theme.white};
      user-select: none;

      &:hover {
        background-color: ${({ theme }) => theme.lightGrey};
      }

      &-active {
        background-color: ${({ theme }) => theme.red};

        &:hover {
          background-color: ${({ theme }) => theme.lightRed};
        }
      }
    }

    &-jump-next,
    &-jump-prev {
      cursor: pointer;
    }

    &-options {
      display: none;
    }

    &-prev,
    &-next {
      display: flex;
      justify-content: center;
      align-items: center;
      width: 30px;
      height: 30px;
      cursor: pointer;

      &:hover {
        & > svg {
          fill: ${({ theme }) => theme.red};
        }
      }

      & > svg {
        fill: ${({ theme }) => theme.grey};
        width: 15px;
        height: 20px;
      }
    }

    &-disabled {
      pointer-events: none;

      & > svg {
        fill: ${({ theme }) => theme.lightestGrey};
      }
    }
  }
`;

export { Wrap };
