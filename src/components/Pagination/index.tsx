import RcPagination, { PaginationProps } from 'rc-pagination';

import { ReactComponent as LeftArrow } from 'assets/chevron-left-icon.svg';
import { ReactComponent as RightArrow } from 'assets/chevron-right-icon.svg';
import { Wrap } from './styled';

interface PaginationProp extends PaginationProps {
  changeCurrentPageAction: (page: number, offset: number) => void;
  getDataAction: (offset: number) => void;
}

export function Pagination({
  current,
  pageSize,
  total,
  changeCurrentPageAction,
  showLessItems = false,
  disabled = false,
  getDataAction = () => {},
}: PaginationProp) {
  const onHandlePage = (page: number, pageSize: number) => {
    const offset = (page - 1) * pageSize;
    changeCurrentPageAction(page, offset);
    getDataAction(offset);
  };

  return (
    <Wrap>
      <RcPagination
        current={current}
        pageSize={pageSize}
        total={total}
        showLessItems={showLessItems}
        prevIcon={<LeftArrow />}
        nextIcon={<RightArrow />}
        disabled={disabled}
        jumpNextIcon="..."
        jumpPrevIcon="..."
        onChange={onHandlePage}
      />
    </Wrap>
  );
}
