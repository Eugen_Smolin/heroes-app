import styled from 'styled-components';

import { device } from 'utils/device';

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  height: 100%;
  max-width: 1100px;
  margin: 0 auto;
  padding: 50px 24px 0;

  @media ${device.lg} {
    padding-top: 25px;
  }
`;

const Main = styled.main`
  flex: 1 1 auto;
  padding-bottom: 50px;
`;

const BgImage = styled.img`
  width: 350px;
  height: 300px;
  position: fixed;
  bottom: 0;
  right: 0;
  z-index: -1;

  @media ${device.lg} {
    display: none;
  }
`;

export { Wrapper, Main, BgImage };
