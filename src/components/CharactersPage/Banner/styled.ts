import styled, { css } from 'styled-components';

import { TextEllipsisForLines } from 'components/Theme/mixins';
import { device } from 'utils/device';

const Wrap = styled.div`
  display: grid;
  grid-template-columns: repeat(2, 1fr);
  height: 250px;
  box-shadow: ${({ theme }) => theme.blackShadow};
  ${({ css }) => css}

  @media ${device.lg} {
    display: block;
    height: auto;
    margin-bottom: 30px;
  }
`;

const LeftSide = styled.div(
  ({ theme }) => css`
    position: relative;
    display: flex;
    column-gap: 30px;
    padding: 30px 30px 30px 35px;
    background-color: ${theme.white};
    height: 100%;

    @media ${device.lg} {
      height: 250px;
    }

    @media ${device.xsm} {
      flex-direction: column;
      min-height: 403px;
      height: auto;
      row-gap: 12px;
      align-items: center;
    }
  `
);

const NotFound = styled.div`
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  font-weight: 700;
  font-size: 18px;
  max-width: 200px;
  text-align: center;
`;

const RightSide = styled.div(
  ({ theme }) => css`
    z-index: 0;
    display: flex;
    flex-direction: column;
    align-items: flex-start;
    position: relative;
    padding: 35px 35px 35px 40px;
    background-color: ${theme.darkGrey};
    color: ${theme.white};
    font-size: 24px;
    font-weight: 700;
    height: 100%;
    overflow: hidden;

    @media ${device.lg} {
      height: 250px;
    }
  `
);

const WeaponsIcon = styled.img`
  position: absolute;
  right: -30px;
  bottom: 10px;
  z-index: -1;
`;

const Title = styled.h1`
  max-width: 374px;
  font-size: inherit;
  font-weight: inherit;
  margin-bottom: auto;
`;

const ImageWrap = styled.div`
  height: 100%;
  width: 180px;
  flex-shrink: 0;

  @media ${device.xsm} {
    height: 180px;
  }
`;

const InfoWrap = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;

  @media ${device.xsm} {
    row-gap: 12px;
  }
`;

const Name = styled.div`
  text-transform: uppercase;
  font-weight: 700;
  font-size: 22px;
  margin-bottom: 10px;

  @media ${device.xsm} {
    ${TextEllipsisForLines({ linesNumber: 1 })};
    margin-bottom: 0;
  }
`;

const Description = styled.div`
  font-size: 14px;
  margin-bottom: auto;
  ${TextEllipsisForLines({ linesNumber: 5 })};

  @media ${device.sm} {
    ${TextEllipsisForLines({ linesNumber: 2 })};
  }

  @media ${device.xsm} {
    ${TextEllipsisForLines({ linesNumber: 1 })};
  }
`;

const ButtonsBlock = styled.div`
  display: flex;
  column-gap: 30px;

  @media ${device.sm} {
    flex-direction: column;
    row-gap: 10px;
  }
`;

export {
  Wrap,
  LeftSide,
  RightSide,
  WeaponsIcon,
  Title,
  ImageWrap,
  Name,
  Description,
  ButtonsBlock,
  InfoWrap,
  NotFound,
};
