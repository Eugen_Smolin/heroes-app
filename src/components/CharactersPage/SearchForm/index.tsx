import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import { useEffect } from 'react';

import { Input } from 'components/Form';
import { Button } from 'components/Button';
import { Status } from 'utils/const';
import { useAppDispatch, useAppSelector } from 'hooks/redux-hook';
import { character } from 'store/characters/character';
import { defaultValues, schema } from './config';
import { Form, HelperText, Title, Wrap } from './styled';

interface IFormValues {
  name: string;
}

export function SearchForm() {
  const dispatch = useAppDispatch();
  const list = useAppSelector(character.selectors.searchCharacters);
  const searchStatus = useAppSelector(character.selectors.searchStatus);

  const {
    handleSubmit,
    control,
    formState: { isSubmitting, isValidating },
  } = useForm<IFormValues>({
    defaultValues,
    resolver: yupResolver(schema),
  });

  const onSubmit = (data: IFormValues) => {
    dispatch(character.thunks.getCharacterByName(data));
  };

  useEffect(() => {
    if (isSubmitting || isValidating) {
      dispatch(character.actions.CLEAR_SEARCH_CHARACTERS());
    }
  }, [isSubmitting, isValidating]);

  useEffect(() => {
    return () => {
      dispatch(character.actions.CLEAR_SEARCH_CHARACTERS());
    };
  }, []);

  return (
    <Form onSubmit={handleSubmit(onSubmit)}>
      <Title>Or find a character by name:</Title>
      <Wrap>
        <Input
          placeholder="Enter name"
          name="name"
          control={control}
        />
        <Button
          loading={searchStatus === Status.PENDING}
          type="submit"
        >
          Find
        </Button>
      </Wrap>
      <HelperText error={!!list && !list.length}>
        {list ? (
          list.length ? (
            <>
              There is! Visit {list[0].name} page?
              <Button
                variant="secondary"
                css={{ flexShrink: 0, flexBasis: '65px' }}
                to={`/characters/${list[0].id}`}
              >
                To page
              </Button>
            </>
          ) : (
            'The character was not found. Check the name and try again'
          )
        ) : null}
      </HelperText>
    </Form>
  );
}
