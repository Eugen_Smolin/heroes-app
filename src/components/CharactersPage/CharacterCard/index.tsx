import { Image } from 'components/Image';
import { ICharacterCustom } from 'types/characters';
import { Wrap, ImgWrap } from './styled';

interface CharacterCardProps
  extends Pick<ICharacterCustom, 'imageHref' | 'name' | 'id'> {
  onClick: (id: number) => void;
}

export function CharacterCard({
  id,
  name,
  imageHref,
  onClick = () => {},
}: CharacterCardProps) {
  const handleClick = () => onClick(id);

  return (
    <Wrap onClick={handleClick}>
      <ImgWrap>
        <Image
          src={imageHref}
          alt={name}
        />
      </ImgWrap>
      <span>{name}</span>
    </Wrap>
  );
}
