import styled from 'styled-components';

import { CardHover } from 'components/Theme/mixins';

const Wrap = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  height: 318px;
  background-color: ${({ theme }) => theme.darkGrey};
  font-size: 22px;
  font-weight: 700;
  text-transform: uppercase;
  color: ${({ theme }) => theme.white};
  transition: 0.2s;
  box-shadow: ${({ theme }) => theme.cardShadow};
  ${CardHover}

  & > span {
    flex: 1 0 auto;
    padding: 15px;
  }
`;

const ImgWrap = styled.div`
  width: 100%;
  height: 200px;
`;

export { Wrap, ImgWrap };
