import styled from 'styled-components';

import { TextEllipsisForLines } from 'components/Theme/mixins';
import { device } from 'utils/device';

const Wrap = styled.div`
  position: relative;
  min-height: 300px;
  padding: 25px;
  box-shadow: ${({ theme }) => theme.shadow};
  background-color: ${({ theme }) => theme.white};

  @media ${device.lg} {
    display: none;
  }
`;

const EmptyText = styled.div`
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  text-align: center;
  color: ${({ theme }) => theme.black};
  font-size: 18px;
  font-weight: 700;
  width: 250px;
`;

const Header = styled.div`
  display: flex;
  column-gap: 25px;
`;

const ImageWrap = styled.div`
  flex-shrink: 0;
  width: 150px;
  height: 150px;
`;

const NameBlock = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
`;

const Name = styled.div`
  font-weight: 700;
  font-size: 22px;
  text-transform: uppercase;
  ${TextEllipsisForLines({ linesNumber: 2 })}
`;

const ButtonBlock = styled.div`
  display: flex;
  flex-direction: column;
  row-gap: 10px;
`;

const Description = styled.div`
  margin-top: 15px;
  font-size: 14px;
`;

export {
  Wrap,
  EmptyText,
  Header,
  ImageWrap,
  Name,
  NameBlock,
  ButtonBlock,
  Description,
};
