import { Image } from 'components/Image';
import { IComicsCustom } from 'types/comics';
import { ImageWrap, Wrap, Title, Price } from './styled';

interface ComicsCardProps
  extends Pick<IComicsCustom, 'imageHref' | 'title' | 'price'> {
  onClick: () => void;
}

export function ComicsCard({
  imageHref,
  title,
  price,
  onClick = () => {},
}: ComicsCardProps) {
  return (
    <Wrap>
      <ImageWrap onClick={onClick}>
        <Image
          src={imageHref}
          alt={title}
        />
      </ImageWrap>
      <Title>{title}</Title>
      <Price>{price}</Price>
    </Wrap>
  );
}
