import styled, { css, keyframes } from 'styled-components';

const Rotate = keyframes`
  0% {
    transform: rotate(0);
  }

  100% {
    transform: rotate(360deg);
  }
`;

const Wrap = styled.div<{ isSmall?: boolean }>`
  position: absolute;
  width: 40px;
  height: 40px;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);

  ${({ isSmall }) =>
    isSmall &&
    css`
      width: 20px;
      height: 20px;

      ${Spinner} {
        border-width: 2px;
      }
    `}
`;

const Spinner = styled.div`
  width: 100%;
  height: 100%;
  border: 6px solid ${({ theme }) => theme.lightGrey};
  border-radius: 50%;
  border-top-color: ${({ theme }) => theme.white};
  opacity: 1;
  transition: opacity 200ms;
  animation: ${Rotate} 1s linear infinite;
  transition-delay: 200ms;
`;

export { Wrap, Spinner };
