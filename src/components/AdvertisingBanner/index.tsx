import BannerIcon from 'assets/banner.jpg';
import { DefaultProps } from 'types/common';
import { Wrap } from './styled';

export function AdvertisingBanner({ css }: DefaultProps<HTMLImageElement>) {
  return (
    <Wrap
      alt="Banner"
      src={BannerIcon}
      css={css}
    />
  );
}
