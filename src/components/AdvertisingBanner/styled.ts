import styled from 'styled-components';

const Wrap = styled.img`
  max-width: 100%;
  ${({ css }) => css}
`;

export { Wrap };
