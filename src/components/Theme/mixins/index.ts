import { css } from 'styled-components';

const TextEllipsisForLines = ({ linesNumber = 1 }) => css`
  overflow: hidden;
  text-overflow: ellipsis;
  display: -webkit-box;
  -webkit-line-clamp: ${linesNumber};
  -webkit-box-orient: vertical;
`;

const CardHover = css`
  &:hover {
    &:hover {
      transform: translateY(-15px);
      cursor: pointer;
      box-shadow: ${({ theme }) => theme.redShadow};
    }
  }
`;

export { TextEllipsisForLines, CardHover };
