import { Wrap } from './styled';

export function EmptyList() {
  return <Wrap>List is empty</Wrap>;
}
