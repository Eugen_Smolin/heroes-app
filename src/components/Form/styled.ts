import styled from 'styled-components';

const Wrap = styled.div``;

const StyledInput = styled.input`
  padding: 10px;
  height: 40px;
  border: none;
  outline: none;
  box-shadow: ${({ theme }) => theme.fieldShadow};
  font-size: 14px;
  width: 100%;

  &::placeholder {
    color: ${({ theme }) => theme.lightDark};
  }
`;

const ErrorText = styled.div`
  color: ${({ theme }) => theme.red};
  font-size: 18px;
  font-weight: 700;
  margin-top: 25px;
`;

export { Wrap, StyledInput, ErrorText };
