import styled from 'styled-components';

const Img = styled.img`
  width: 100%;
  height: 100%;
  object-fit: cover;
  object-position: center;
  user-select: none;
  ${({ css }) => css};
`;

export { Img };
