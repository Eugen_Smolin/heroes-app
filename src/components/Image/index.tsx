import { useState } from 'react';

import { SkeletonLoader } from 'components/SkeletonLoader';
import { DefaultProps } from 'types/common';
import { Img } from './styled';

interface ImageProps extends DefaultProps<HTMLImageElement> {}

export function Image({ src, alt, css }: ImageProps) {
  const [isLoaded, setIsLoaded] = useState(false);

  const handleLoad = () => setIsLoaded(true);

  return (
    <SkeletonLoader isLoading={!isLoaded}>
      <Img
        src={src}
        alt={alt}
        css={css}
        draggable={false}
        onLoad={handleLoad}
      />
    </SkeletonLoader>
  );
}
