export const WEB_API_ROUTES = {
  characters: '/characters',
  characterById: '/characters/{characterId}',
  comics: '/comics',
  comicById: '/comics/{comicId}',
};
