import http from 'api/http';
import { WEB_API_ROUTES } from 'api/apiRoutes';
import { IPaginationParams } from 'types/common';

const api = {
  characters: {
    getAllCharacters(params: IPaginationParams) {
      return http.get(WEB_API_ROUTES.characters, { params });
    },
    getCharacterByName(params: { name: string }) {
      return http.get(WEB_API_ROUTES.characters, { params });
    },
    getCharacterById(id: number | string) {
      return http.get(
        WEB_API_ROUTES.characterById.replace('{characterId}', `${id}`)
      );
    },
  },
  comics: {
    getAllComics(params: IPaginationParams) {
      return http.get(WEB_API_ROUTES.comics, { params });
    },
    getComicById(id: number | string) {
      return http.get(WEB_API_ROUTES.comicById.replace('{comicId}', `${id}`));
    },
  },
};

export default api;
