import 'styled-components';
import { CustomThemeType } from './components/Theme';
import { CSSObject, CSSProp } from 'styled-components';

declare module 'styled-components' {
  interface DefaultTheme extends CustomThemeType {}
}

declare module 'react' {
  interface Attributes {
    css?: CSSProp | CSSObject;
  }
}
