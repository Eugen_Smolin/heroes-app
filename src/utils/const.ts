enum Status {
  IDLE = 'idle',
  PENDING = 'pending',
  SUCCESS = 'success',
  FAIL = 'fail',
}

enum ImageSize {
  STANDARD_XLARGE = 'standard_xlarge', // 200*200
  PORTRAIT_UNCANNY = 'portrait_uncanny', // 300 * 450
  PORTRAIT_INCREDIBLE = 'portrait_incredible', // 216 * 324
}

export { Status, ImageSize };
