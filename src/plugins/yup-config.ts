import * as yup from 'yup';

const validationMessages = {
  mixed: {
    required: 'This field is required',
  },
};

yup.setLocale(validationMessages);

export default yup;
