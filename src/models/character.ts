import { ICharacterCustom, ICharacterOrig } from 'types/characters';
import { ImageSize } from '../utils/const';

const characterModel = (character: ICharacterOrig): ICharacterCustom => {
  const { thumbnail, urls, id, name, description } = character;
  const imageHref =
    thumbnail.path + `/${ImageSize.STANDARD_XLARGE}.${thumbnail.extension}`;
  const customDescription = description.length
    ? description
    : 'No description. For details go to the wiki page';

  return {
    id,
    name,
    description: customDescription,
    imageHref,
    wikiUrl: urls.find(({ type }) => type === 'detail')?.url,
  };
};

export { characterModel };
