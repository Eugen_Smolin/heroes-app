const pagination = ({ total = 6, perPage = 6, offset = 0 }) => {
  return {
    current: offset === 0 ? 1 : offset / perPage + 1,
    offset,
    total,
    pageSize: perPage,
  };
};

export { pagination };
