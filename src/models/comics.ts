import { IComicsCustom, IComicsOrig } from 'types/comics';
import { ImageSize } from 'utils/const';

const comicsModel = (
  comics: IComicsOrig,
  imageSize: Exclude<ImageSize, ImageSize.STANDARD_XLARGE>
): IComicsCustom => {
  const { id, title, description, thumbnail, prices, pageCount, textObjects } =
    comics;
  const imageHref = thumbnail.path + `/${imageSize}.${thumbnail.extension}`;
  const customDescription =
    description && description.length
      ? description
      : 'No description. For details go to the wiki page';
  const language = textObjects[0]?.language
    ? textObjects[0].language
    : 'Unknown';
  const price = prices[0].price ? `${prices[0].price}$` : 'Not Available';

  return {
    id,
    title,
    description: customDescription,
    imageHref,
    price,
    pageCount,
    language,
  };
};

export { comicsModel };
