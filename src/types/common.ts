import {
  ButtonHTMLAttributes,
  DetailedHTMLProps,
  HTMLAttributes,
  ImgHTMLAttributes,
  InputHTMLAttributes,
} from 'react';

import { pagination } from 'models/pagination';

type Attributes<T> = T extends HTMLButtonElement
  ? ButtonHTMLAttributes<T>
  : T extends HTMLInputElement
  ? InputHTMLAttributes<T>
  : T extends HTMLImageElement
  ? ImgHTMLAttributes<T>
  : HTMLAttributes<T>;
type DefaultProps<T> = DetailedHTMLProps<Attributes<T>, T>;

interface IUrl {
  type: string;
  url: string;
}

interface IThumbnail {
  path: string;
  extension: string;
}

interface IDataWithMeta<T> {
  results: T;
  offset: number;
  total: number;
  limit: number;
  count: number;
}

type IPagination = ReturnType<typeof pagination>;

interface IPaginationParams {
  offset: number;
  limit?: number;
}

export type {
  DefaultProps,
  IUrl,
  IThumbnail,
  IDataWithMeta,
  IPagination,
  IPaginationParams,
};
