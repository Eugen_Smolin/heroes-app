import { createAsyncThunk } from '@reduxjs/toolkit';
import { AxiosError } from 'axios';

import api from 'api';
import { handleServerErrors } from 'utils/serverErrors';
import { IDataWithMeta, IPaginationParams } from 'types/common';
import { ICharacterOrig } from 'types/characters';
import { RootState } from 'store';

const getAllCharacters = createAsyncThunk(
  'characters/getAllCharacters',
  async (params: IPaginationParams, { getState }) => {
    try {
      const limit: number = (getState() as RootState).characters.pagination
        .pageSize;
      const { data } = await api.characters.getAllCharacters({
        limit,
        ...params,
      });
      return data.data as IDataWithMeta<ICharacterOrig[]>;
    } catch (err) {
      if (err instanceof AxiosError) {
        handleServerErrors(err);
        throw err;
      }
    }
  }
);

export const thunks = { getAllCharacters };
