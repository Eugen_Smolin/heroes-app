import { createAsyncThunk } from '@reduxjs/toolkit';
import { AxiosError } from 'axios';

import api from 'api';
import { handleServerErrors } from 'utils/serverErrors';
import { ICharacterOrig } from 'types/characters';

const getRandomCharacter = createAsyncThunk(
  'characters/getRandomCharacter',
  async (id: number) => {
    try {
      const { data } = await api.characters.getCharacterById(id);
      return data.data.results[0] as ICharacterOrig;
    } catch (err) {
      if (err instanceof AxiosError) {
        handleServerErrors(err);
        throw err;
      }
    }
  }
);

const getCharacterById = createAsyncThunk(
  'characters/getCharacterById',
  async (id: string | number) => {
    try {
      const { data } = await api.characters.getCharacterById(id);
      return data.data.results[0] as ICharacterOrig;
    } catch (err) {
      if (err instanceof AxiosError) {
        handleServerErrors(err);
        throw err;
      }
    }
  }
);

const getCharacterByName = createAsyncThunk(
  'characters/getCharacterByName',
  async (params: { name: string }) => {
    try {
      const { data } = await api.characters.getCharacterByName(params);
      return data.data.results as ICharacterOrig[];
    } catch (err) {
      if (err instanceof AxiosError) {
        handleServerErrors(err);
        throw err;
      }
    }
  }
);

export const thunks = {
  getCharacterById,
  getRandomCharacter,
  getCharacterByName,
};
