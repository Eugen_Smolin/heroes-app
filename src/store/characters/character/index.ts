import { createSlice } from '@reduxjs/toolkit';

import { characterModel } from 'models/character';
import { ICharacterCustom } from 'types/characters';
import { Status } from 'utils/const';
import { thunks } from './thunks';
import { selectors } from './selectors';

interface IState {
  randomCharacter: ICharacterCustom | null;
  character: ICharacterCustom | null;
  searchCharacters: ICharacterCustom[] | null;
  characterFetchingStatus: Status;
  randomFetchingStatus: Status;
  searchStatus: Status;
}

const initialState: IState = {
  character: null,
  randomCharacter: null,
  searchCharacters: null,
  characterFetchingStatus: Status.IDLE,
  randomFetchingStatus: Status.IDLE,
  searchStatus: Status.IDLE,
};

const slice = createSlice({
  name: 'character',
  initialState,
  reducers: {
    CLEAR_SEARCH_CHARACTERS: (state) => {
      state.searchCharacters = initialState.searchCharacters;
      state.searchStatus = initialState.searchStatus;
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(thunks.getCharacterById.pending, (state) => {
        state.characterFetchingStatus = Status.PENDING;
      })
      .addCase(thunks.getCharacterById.fulfilled, (state, { payload }) => {
        if (payload) {
          state.character = characterModel(payload);
        }
        state.characterFetchingStatus = Status.SUCCESS;
      })
      .addCase(thunks.getCharacterById.rejected, (state) => {
        state.characterFetchingStatus = Status.FAIL;
      })
      .addCase(thunks.getRandomCharacter.pending, (state) => {
        state.randomFetchingStatus = Status.PENDING;
      })
      .addCase(thunks.getRandomCharacter.fulfilled, (state, { payload }) => {
        if (payload) {
          state.randomCharacter = characterModel(payload);
        }
        state.randomFetchingStatus = Status.SUCCESS;
      })
      .addCase(thunks.getRandomCharacter.rejected, (state) => {
        state.randomFetchingStatus = Status.FAIL;
      })
      .addCase(thunks.getCharacterByName.pending, (state) => {
        state.searchStatus = Status.PENDING;
      })
      .addCase(thunks.getCharacterByName.fulfilled, (state, { payload }) => {
        if (payload) {
          state.searchCharacters = payload.map((character) =>
            characterModel(character)
          );
        }
        state.searchStatus = Status.SUCCESS;
      })
      .addCase(thunks.getCharacterByName.rejected, (state) => {
        state.searchStatus = Status.FAIL;
      });
  },
});

export const character = {
  actions: slice.actions,
  selectors,
  thunks,
};
export default slice.reducer;
