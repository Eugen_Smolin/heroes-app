import { RootState } from 'store';

export const selectors = {
  character: (state: RootState) => state.character.character,
  randomCharacter: (state: RootState) => state.character.randomCharacter,
  searchCharacters: (state: RootState) => state.character.searchCharacters,
  characterFetchingStatus: (state: RootState) =>
    state.character.characterFetchingStatus,
  randomFetchingStatus: (state: RootState) =>
    state.character.randomFetchingStatus,
  searchStatus: (state: RootState) => state.character.searchStatus,
};
