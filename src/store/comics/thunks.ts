import { createAsyncThunk } from '@reduxjs/toolkit';
import { AxiosError } from 'axios';

import api from 'api';
import { handleServerErrors } from 'utils/serverErrors';
import { IComicsOrig } from 'types/comics';
import { IDataWithMeta, IPaginationParams } from 'types/common';
import { RootState } from 'store';

const getAllComics = createAsyncThunk(
  'comics/getAllComics',
  async (params: IPaginationParams, { getState }) => {
    try {
      const limit: number = (getState() as RootState).comics.pagination
        .pageSize;
      const { data } = await api.comics.getAllComics({ ...params, limit });
      return data.data as IDataWithMeta<IComicsOrig[]>;
    } catch (err) {
      if (err instanceof AxiosError) {
        handleServerErrors(err);
        throw err;
      }
    }
  }
);

export const thunks = { getAllComics };
