import { RootState } from 'store';

export const selectors = {
  list: (state: RootState) => state.comics.list,
  pagination: (state: RootState) => state.comics.pagination,
  fetchingStatus: (state: RootState) => state.comics.fetchingStatus,
};
