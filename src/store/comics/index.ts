import { createSlice } from '@reduxjs/toolkit';

import { pagination } from 'models/pagination';
import { comicsModel } from 'models/comics';
import { IComicsCustom } from 'types/comics';
import { IPagination } from 'types/common';
import { ImageSize, Status } from 'utils/const';
import { selectors } from './selectors';
import { thunks } from './thunks';

interface IState {
  list: IComicsCustom[];
  pagination: IPagination;
  fetchingStatus: Status;
}

const initialState: IState = {
  list: [],
  pagination: pagination({ perPage: 8 }),
  fetchingStatus: Status.IDLE,
};

const slice = createSlice({
  name: 'comics',
  initialState,
  reducers: {
    SET_PAGINATION: (state, { payload }) => {
      state.pagination.current = payload.page;
      state.pagination.offset = payload.offset;
    },
  },
  extraReducers: (builder) =>
    builder
      .addCase(thunks.getAllComics.pending, (state) => {
        state.fetchingStatus = Status.PENDING;
      })
      .addCase(thunks.getAllComics.fulfilled, (state, { payload }) => {
        if (payload) {
          const { results, limit, offset, total } = payload;
          state.list = results.map((comics) =>
            comicsModel(comics, ImageSize.PORTRAIT_INCREDIBLE)
          );
          state.pagination = pagination({ total, offset, perPage: limit });
        }
        state.fetchingStatus = Status.SUCCESS;
      })
      .addCase(thunks.getAllComics.rejected, (state) => {
        state.fetchingStatus = Status.FAIL;
      }),
});

export const comics = {
  actions: slice.actions,
  selectors,
  thunks,
};
export default slice.reducer;
